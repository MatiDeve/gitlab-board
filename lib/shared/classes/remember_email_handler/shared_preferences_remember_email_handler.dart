import 'package:gitlab_board/shared/classes/local_store/local_store.dart';
import 'package:gitlab_board/shared/classes/remember_email_handler/remember_email_hander.dart';

class SharedPreferencesRememberEmailHandler implements RememberEmailHandler {
  final LocalStore _localStore;

  SharedPreferencesRememberEmailHandler(this._localStore);

  final String key = "email";

  @override
  Future<String> get() async {
    return await _localStore.getString(key);
  }

  @override
  void set(String email) {
    _localStore.setString(key, email);
  }
}

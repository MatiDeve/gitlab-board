abstract class RememberEmailHandler {
  Future<String> get();

  void set(String token);
}

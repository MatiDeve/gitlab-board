import 'package:flutter/services.dart';
import 'package:url_launcher/url_launcher.dart';

class OpenUrl {
  static Future<bool> open(String url) async {
    if (await canLaunch(url)) {
      await launch(url, forceSafariVC: false);
      return true;
    } else {
      Clipboard.setData(new ClipboardData(text: url));
      return false;
    }
  }
}

import '../classes/applier.dart';
import '../classes/decoder_response.dart';
import '../classes/plain_text_response.dart';
import 'decoder.dart';

class UrlDecoder implements Decoder {
  final String opener = "[url=";
  final String closer = "[/url]";

  @override
  Applier getApplier(String plainText) {
    int startIndex = plainText.indexOf(opener);
    int endIndex = plainText.indexOf(closer);
    bool isApplicable =
        (startIndex >= 0 && endIndex >= 0 && startIndex < endIndex);

    return Applier(this, isApplicable, startIndex, endIndex);
  }

  @override
  DecoderResponse apply(String plainText, Applier applier) {
    PlainTextResponse decoded = _getResponse(
      plainText.substring(applier.startIndex, applier.endIndex + closer.length),
    );

    return DecoderResponse(
        decoded, plainText.substring(applier.endIndex + closer.length));
  }

  PlainTextResponse _getResponse(String plainText) {
    int separatorIndex = plainText.indexOf("]");
    String url = plainText.substring(opener.length, separatorIndex);
    String text = plainText.substring(
        separatorIndex + 1, plainText.length - closer.length);

    return PlainTextResponse(PlainTextType.url, text, url);
  }
}

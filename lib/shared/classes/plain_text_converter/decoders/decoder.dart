import '../classes/applier.dart';
import '../classes/decoder_response.dart';

abstract class Decoder {
  Applier getApplier(String plainText);

  DecoderResponse apply(String plainText, Applier applier);
}

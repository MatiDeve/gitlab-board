import 'classes/applier.dart';
import 'classes/decoder_response.dart';
import 'classes/plain_text_response.dart';
import 'decoders/decoder.dart';
import 'decoders/img_decoder.dart';
import 'decoders/url_decoder.dart';

class PlainTextConverter {
  static PlainTextConverter shared =
      PlainTextConverter([ImgDecoder(), UrlDecoder()]);

  final List<Decoder> decoders;

  PlainTextConverter(this.decoders);

  List<PlainTextResponse> decode(String plainText,
      [List<PlainTextResponse> list]) {
    if (list == null) list = List<PlainTextResponse>();

    if (plainText.isEmpty) {
      return list;
    } else {
      List<Applier> appliers = _getApplicablesStorted(plainText);

      if (appliers.isEmpty) {
        list.add(PlainTextResponse(PlainTextType.text, plainText, null));
        return list;
      }

      Applier applier = appliers.first;

      if (_isThereTextBeforeApplier(applier))
        list.add(PlainTextResponse(
            PlainTextType.text, _textBeforeApplier(plainText, applier), null));

      DecoderResponse response = applier.decoder.apply(plainText, applier);
      list.add(response.decoded);
      return decode(response.remainedText, list);
    }
  }

  String _textBeforeApplier(String plainText, Applier applier) =>
      plainText.substring(0, applier.startIndex);

  bool _isThereTextBeforeApplier(Applier applier) => applier.startIndex != 0;

  List<Applier> _getApplicablesStorted(String plainText) {
    List<Applier> appliers = decoders
        .map((d) => d.getApplier(plainText))
        .where((a) => a.isApplicable)
        .toList();
    appliers.sort((a, b) => _getFirstApplicableBetween(a, b));
    return appliers;
  }

  int _getFirstApplicableBetween(Applier a, Applier b) =>
      a.startIndex.compareTo(b.startIndex);
}

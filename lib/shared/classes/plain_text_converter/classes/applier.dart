import '../decoders/decoder.dart';

class Applier {
  final Decoder decoder;
  final bool isApplicable;
  final int startIndex;
  final int endIndex;

  Applier(this.decoder, this.isApplicable, this.startIndex, this.endIndex);
}

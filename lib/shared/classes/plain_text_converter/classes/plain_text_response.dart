enum PlainTextType { text, url, img }

class PlainTextResponse {
  final PlainTextType type;
  final String text;
  final String url;

  PlainTextResponse(this.type, this.text, this.url);
}

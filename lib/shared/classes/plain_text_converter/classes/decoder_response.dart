import 'plain_text_response.dart';

class DecoderResponse {
  final PlainTextResponse decoded;
  final String remainedText;

  DecoderResponse(this.decoded, this.remainedText);
}

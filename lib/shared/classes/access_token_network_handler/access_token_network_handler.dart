import 'package:gitlab_board/shared/classes/network_handler/classes/network_request.dart';
import 'package:gitlab_board/shared/classes/network_handler/network_handler.dart';
import 'package:gitlab_board/shared/classes/token_handler/token_handler.dart';
import 'package:http/http.dart' as http;

class AccessTokenNetworkHandler {
  final NetworkHandler _networkHandler;
  final TokenHandler _tokenHandler;

  AccessTokenNetworkHandler(this._networkHandler, this._tokenHandler);

  Future<http.Response> invoke(NetworkRequest r) async {
    String token = await _tokenHandler.get();
    Map<String, String> updatedHeaders = Map.from(r.headers);
    updatedHeaders["PRIVATE-TOKEN"] = token;
    return _networkHandler.invoke(
      r.copyWith(headers: updatedHeaders),
    );
  }
}

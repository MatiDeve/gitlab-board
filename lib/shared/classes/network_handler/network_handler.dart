import 'dart:convert';

import 'package:http/http.dart' as http;

import 'classes/network_method.dart';
import 'classes/network_request.dart';

class NetworkHandler {
  final String baseUrl;

  NetworkHandler(this.baseUrl);

  Future<http.Response> invoke(NetworkRequest r) async {
    http.Response response = await _getResponse(r);
    print("Uri: ${response.request.url}");
//    print("Headers: ${r.headers}");
    print("Status code: ${response.statusCode}");
    return response;
  }

  Future<http.Response> _getResponse(r) {
    switch (r.method) {
      case NetworkMethod.GET:
        return _get(r);
      case NetworkMethod.POST:
        return _post(r);
      default:
        return null;
    }
  }

  Future<http.Response> _get(NetworkRequest r) async =>
      await http.get(baseUrl + r.endpoint, headers: r.headers);

  Future<http.Response> _post(NetworkRequest r) async =>
      await http.post(baseUrl + r.endpoint,
          body: json.encode(r.data), headers: r.headers);
}

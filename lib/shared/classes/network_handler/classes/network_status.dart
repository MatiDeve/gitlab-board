enum NetworkStatus {
  OK,
  CREATED,
  NOT_FOUND,
}

extension NetworkStatusExtension on NetworkStatus {
  int get value {
    switch (this) {
      case NetworkStatus.OK:
        return 200;
      case NetworkStatus.CREATED:
        return 201;
      case NetworkStatus.NOT_FOUND:
        return 404;
      default:
        return 500;
    }
  }
}

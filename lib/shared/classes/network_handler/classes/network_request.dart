import 'package:flutter/material.dart';

import 'network_method.dart';

class NetworkRequest {
  final String endpoint;
  final Map<String, String> headers;
  final Map<String, dynamic> data;
  final NetworkMethod method;

  NetworkRequest(
      {@required this.endpoint,
      this.headers = const {"Content-Type": "application/json"},
      this.data,
      @required this.method});

  NetworkRequest copyWith({Map<String, String> headers}) => NetworkRequest(
        endpoint: this.endpoint,
        headers: headers ?? this.headers,
        data: this.data,
        method: this.method,
      );
}

abstract class TokenHandler {
  void set(String token);

  Future<String> get();

  Future<bool> exists();

  void destroy();
}

import 'package:gitlab_board/shared/classes/local_store/local_store.dart';
import 'package:gitlab_board/shared/classes/token_handler/token_handler.dart';

class SharedPreferencesTokenHandler implements TokenHandler {
  final String key = "token";

  final LocalStore _localStore;

  SharedPreferencesTokenHandler(this._localStore);

  @override
  void destroy() {
    _localStore.setString(key, null);
  }

  @override
  Future<String> get() async {
    return await _localStore.getString(key);
  }

  @override
  void set(String token) {
    _localStore.setString(key, token);
  }

  @override
  Future<bool> exists() async {
    return await _localStore.getString(key) != null;
  }
}

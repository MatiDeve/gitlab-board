import 'rules/day.dart';
import 'rules/hour.dart';
import 'rules/minute.dart';
import 'rules/rule.dart';
import 'rules/second.dart';

class TimeDifference {
  final List<TimeDifferenceRule> rules = [
    DayDifferenceRule(),
    HourDifferenceRule(),
    MinuteDifferenceRule(),
    SecondDifferenceRule()
  ];

  String get(DateTime d) =>
      _getDifferenceTimeForDuration(DateTime.now().difference(d));

  String _getDifferenceTimeForDuration(Duration d) =>
      rules.firstWhere((rule) => rule.hasToApply(d)).text(d);
}

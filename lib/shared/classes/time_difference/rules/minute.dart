import 'rule.dart';

class MinuteDifferenceRule implements TimeDifferenceRule {
  @override
  bool hasToApply(Duration d) => d.inMinutes > 0;

  @override
  String text(Duration d) =>
      d.inMinutes == 1 ? "Hace un minuto." : "Hace ${d.inMinutes} minutos.";
}

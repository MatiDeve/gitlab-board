import 'rule.dart';

class HourDifferenceRule implements TimeDifferenceRule {
  @override
  bool hasToApply(Duration d) => d.inHours > 0;

  @override
  String text(Duration d) =>
      d.inHours == 1 ? "Hace una hora." : "Hace ${d.inHours} horas.";
}

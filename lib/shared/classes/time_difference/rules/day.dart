import 'rule.dart';

class DayDifferenceRule implements TimeDifferenceRule {
  @override
  bool hasToApply(Duration d) => d.inDays > 0;

  @override
  String text(Duration d) =>
      d.inDays == 1 ? "Hace un día." : "Hace ${d.inDays} días.";
}

abstract class TimeDifferenceRule {
  bool hasToApply(Duration d);

  String text(Duration d);
}

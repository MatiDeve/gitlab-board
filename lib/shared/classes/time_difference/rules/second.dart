import 'rule.dart';

class SecondDifferenceRule implements TimeDifferenceRule {
  @override
  bool hasToApply(Duration d) => true;

  @override
  String text(Duration d) =>
      d.inSeconds <= 20 ? "Hace un instante." : "Hace ${d.inSeconds} segundos.";
}

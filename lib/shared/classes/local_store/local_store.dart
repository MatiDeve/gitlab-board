abstract class LocalStore {
  Future<String> getString(String key);

  Future<int> getInt(String key);

  void setString(String key, String value);

  void setInt(String key, int value);

  Future<bool> exists(String key);
}

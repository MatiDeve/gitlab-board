import 'package:shared_preferences/shared_preferences.dart';

import 'local_store.dart';

class SharedPreferencesLocalStore implements LocalStore {
  @override
  Future<String> getString(String key) async {
    return await _getStringValueFor(key);
  }

  @override
  Future<int> getInt(String key) async {
    return await _getIntValueFor(key);
  }

  @override
  void setString(String key, String value) async {
    SharedPreferences s = await SharedPreferences.getInstance();
    s.setString(key, value);
  }

  @override
  void setInt(String key, int value) async {
    SharedPreferences s = await SharedPreferences.getInstance();
    s.setInt(key, value);
  }

  @override
  Future<bool> exists(String key) async {
    return await _getStringValueFor(key) != null;
  }

  Future<String> _getStringValueFor(String key) async {
    SharedPreferences s = await SharedPreferences.getInstance();
    return s.getString(key);
  }

  Future<int> _getIntValueFor(String key) async {
    SharedPreferences s = await SharedPreferences.getInstance();
    return s.getInt(key);
  }
}

abstract class UserHandler {
  void set(User u);

  Future<User> get();

  Future<bool> exists();

  void destroy();
}

class User {
  final int id;
  final String name;
  final String username;

  User(this.id, this.name, this.username);

  bool exists() => id != null && name.isNotEmpty && username.isNotEmpty;

  void printConsole() {
    print("USER ID: $id");
    print("USER NAME: $name");
    print("USER USERNAME: $username");
  }
}

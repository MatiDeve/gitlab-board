import 'package:gitlab_board/shared/classes/local_store/local_store.dart';
import 'package:gitlab_board/shared/classes/user_handler/user_handler.dart';

class SharedPreferencesUserHandler implements UserHandler {
  final String idKey = "user_id";
  final String nameKey = "user_name";
  final String usernameKey = "user_username";

  final LocalStore _localStore;

  SharedPreferencesUserHandler(this._localStore);

  @override
  void destroy() {
    _localStore.setString(idKey, null);
    _localStore.setString(usernameKey, null);
    _localStore.setString(nameKey, null);
  }

  @override
  Future<User> get() async {
    int id = await _localStore.getInt(idKey);
    String name = await _localStore.getString(nameKey);
    String username = await _localStore.getString(usernameKey);
    var user = User(id, name, username);
    return user;
  }

  @override
  void set(User u) {
    _localStore.setInt(idKey, u.id);
    _localStore.setString(usernameKey, u.username);
    _localStore.setString(nameKey, u.name);
  }

  @override
  Future<bool> exists() async {
    return (await this.get()).exists();
  }
}

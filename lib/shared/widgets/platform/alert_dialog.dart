import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class PlatformAlertDialog extends StatelessWidget {
  Widget title;
  Widget content;
  List<Widget> actions;

  PlatformAlertDialog({this.title, this.content, this.actions});

  @override
  Widget build(BuildContext context) {
    return Theme.of(context).platform == TargetPlatform.iOS
        ? CupertinoAlertDialog(
            title: title,
            content: content,
            actions: actions == null ? const <Widget>[] : actions)
        : AlertDialog(
            title: title,
            content: content,
            actions: actions,
          );
  }
}

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class PlatformButton extends StatelessWidget {
  final Widget child;
  final Function onPressed;
  final Color color;

  PlatformButton({this.child, this.onPressed, this.color});

  @override
  Widget build(BuildContext context) {
    return Theme.of(context).platform == TargetPlatform.iOS
        ? CupertinoButton(
            child: child,
            onPressed: onPressed,
            color: color == null ? Theme.of(context).accentColor : color)
        : RaisedButton(
            child: child,
            onPressed: onPressed,
            color: color == null ? Theme.of(context).accentColor : color);
  }
}

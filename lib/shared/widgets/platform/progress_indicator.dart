import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class PlatformProgressIndicator extends StatelessWidget {
  final Color color;
  final double width;

  PlatformProgressIndicator({this.color, this.width = 4.0});

  @override
  Widget build(BuildContext context) {
    return Theme.of(context).platform == TargetPlatform.iOS
        ? CupertinoActivityIndicator()
        : CircularProgressIndicator(
            strokeWidth: width,
            valueColor: new AlwaysStoppedAnimation<Color>(
                color != null ? color : Theme.of(context).accentColor),
          );
  }
}

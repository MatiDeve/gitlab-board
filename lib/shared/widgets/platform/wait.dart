import 'package:flutter/material.dart';

import './alert_dialog.dart';
import './progress_indicator.dart';

void buildWaitMessage(BuildContext context, {Color color}) {
  showDialog(
      barrierDismissible: false,
      context: context,
      builder: (BuildContext context) {
        return WillPopScope(
          child: PlatformAlertDialog(
            title: Row(
              children: <Widget>[
                PlatformProgressIndicator(color: color),
                SizedBox(width: 16.0),
                Flexible(
                  child: Text(
                    "Espere...",
                    overflow: TextOverflow.ellipsis,
                  ),
                )
              ],
            ),
          ),
          onWillPop: () {},
        );
      });
}

import 'package:flutter/material.dart';
import 'package:gitlab_board/shared/classes/open_url.dart';

class Link extends StatelessWidget {
  final String text;
  final String url;

  const Link({
    Key key,
    @required this.text,
    @required this.url,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () async {
        if (!await OpenUrl.open(url)) {
          final snackBar = SnackBar(
            content: Text("Link copiado"),
            action: SnackBarAction(
              label: 'Ok',
              onPressed: () {
                // Some code to undo the change!
              },
            ),
          );
          Scaffold.of(context).showSnackBar(snackBar);
        }
      },
      child: Text(
        text,
        style: TextStyle(
          color: Theme.of(context).primaryColor,
          fontWeight: FontWeight.w500,
        ),
      ),
    );
  }
}

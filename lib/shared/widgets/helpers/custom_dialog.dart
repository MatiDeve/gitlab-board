import 'package:flutter/material.dart';

class CustomDialog extends StatelessWidget {
  final String title;
  final String content;

  const CustomDialog(
      {Key key,
      this.title = "¡Ups!",
      this.content = "Ha ocurrido un error. Inténtenlo nuevamente."})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Dialog(
      shape: const RoundedRectangleBorder(
        borderRadius: BorderRadius.all(Radius.circular(16.0)),
      ),
      child: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            buildTitle(context),
            SizedBox(height: 12),
            buildContent(context),
            buildButtonsContainer(context)
          ],
        ),
      ),
    );
  }

  Container buildButtonsContainer(BuildContext context) {
    return Container(
      width: double.infinity,
      child: Align(
        alignment: Alignment.centerRight,
        child: buildDoneButton(context),
      ),
    );
  }

  Text buildTitle(BuildContext context) {
    return Text(
      title,
      style: Theme.of(context).textTheme.title,
    );
  }

  Text buildContent(BuildContext context) {
    return Text(
      content,
      style: Theme.of(context).textTheme.subtitle,
    );
  }

  Widget buildDoneButton(BuildContext context) {
    return FlatButton(
      child: Text(
        "Aceptar",
        style: TextStyle(
          color: Theme.of(context).accentColor,
        ),
      ),
      onPressed: () {
        Navigator.of(context).pop();
      },
    );
  }
}

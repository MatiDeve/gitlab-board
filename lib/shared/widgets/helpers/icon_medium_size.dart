import 'package:flutter/material.dart';

class IconMediumSize extends StatelessWidget {
  final String assetPath;

  const IconMediumSize({Key key, @required this.assetPath}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 200.0,
      height: 200.0,
      child: Image.asset(assetPath),
    );
  }
}

import 'package:flutter/material.dart';
import 'package:gitlab_board/shared/widgets/forms/inputs/input_properties.dart';
import 'package:gitlab_board/shared/widgets/forms/inputs/validators.dart';
import 'package:line_icons/line_icons.dart';

class PasswordInputProperties extends InputProperties {
  @override
  bool get isSecret => true;

  @override
  TextInputType get keyboardType => TextInputType.text;

  @override
  String get placeholder => "Contraseña";

  @override
  IconData get prefixIcon => LineIcons.lock_solid;

  @override
  Function get validator => InputValidators.passwordValidator;

  @override
  TextCapitalization get textCapitalization => TextCapitalization.none;
}

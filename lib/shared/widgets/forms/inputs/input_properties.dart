import 'package:flutter/material.dart';

class InputProperties {
  final Function validator;
  final TextInputType keyboardType;
  final String placeholder;
  final IconData prefixIcon;
  final bool isSecret;
  final TextCapitalization textCapitalization;

  InputProperties(
      {this.validator,
      this.keyboardType,
      this.placeholder,
      this.prefixIcon,
      this.isSecret,
      this.textCapitalization});

  InputProperties copyWith(
          {Function validator,
          TextInputType keyboardType,
          String placeholder,
          IconData prefixIcon,
          bool isSecret,
          TextCapitalization textCapitalization}) =>
      InputProperties(
        validator: validator ?? this.validator,
        keyboardType: keyboardType ?? this.keyboardType,
        placeholder: placeholder ?? this.placeholder,
        prefixIcon: prefixIcon ?? this.prefixIcon,
        isSecret: isSecret ?? this.isSecret,
        textCapitalization: textCapitalization ?? this.textCapitalization,
      );
}

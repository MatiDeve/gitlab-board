import 'package:flutter/material.dart';
import 'package:gitlab_board/shared/widgets/forms/inputs/input_properties.dart';
import 'package:gitlab_board/shared/widgets/forms/inputs/validators.dart';
import 'package:line_icons/line_icons.dart';

class AccessTokenInputProperties extends InputProperties {
  @override
  bool get isSecret => false;

  @override
  TextInputType get keyboardType => TextInputType.text;

  @override
  String get placeholder => "Access token";

  @override
  IconData get prefixIcon => LineIcons.key_solid;

  @override
  Function get validator => InputValidators.defaultValidator;

  @override
  TextCapitalization get textCapitalization => TextCapitalization.none;
}

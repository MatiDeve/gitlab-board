import 'package:flutter/material.dart';
import 'package:gitlab_board/shared/widgets/forms/inputs/input_properties.dart';
import 'package:gitlab_board/shared/widgets/forms/inputs/validators.dart';
import 'package:line_icons/line_icons.dart';

class DefaultInputProperties extends InputProperties {
  @override
  bool get isSecret => false;

  @override
  TextInputType get keyboardType => TextInputType.text;

  @override
  String get placeholder => "Nombre";

  @override
  IconData get prefixIcon => LineIcons.user;

  @override
  Function get validator => InputValidators.defaultValidator;

  @override
  TextCapitalization get textCapitalization => TextCapitalization.words;
}

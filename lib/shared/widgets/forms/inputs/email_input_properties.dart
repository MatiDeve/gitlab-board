import 'package:flutter/material.dart';
import 'package:gitlab_board/shared/widgets/forms/inputs/input_properties.dart';
import 'package:gitlab_board/shared/widgets/forms/inputs/validators.dart';
import 'package:line_icons/line_icons.dart';

class EmailInputProperties extends InputProperties {
  @override
  bool get isSecret => false;

  @override
  TextInputType get keyboardType => TextInputType.emailAddress;

  @override
  String get placeholder => "Email";

  @override
  IconData get prefixIcon => LineIcons.envelope;

  @override
  Function get validator => InputValidators.emailValidator;

  @override
  TextCapitalization get textCapitalization => TextCapitalization.none;
}

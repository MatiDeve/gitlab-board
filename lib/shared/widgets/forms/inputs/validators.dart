class InputValidators {
  static String defaultValidator(String value) {
    if (value.isEmpty) {
      return "Campo requerido.";
    }
    return null;
  }

  static String emailValidator(String value) {
    if (value.isEmpty) {
      return "El email es requerido.";
    }
    if (!RegExp(
            r"[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?")
        .hasMatch(value)) {
      return "El email es invalido.";
    }
    return null;
  }

  static String passwordValidator(String value) {
    if (value.isEmpty) return "La contraseña es requerida.";
    if (value.length < 6) return "Caracteres mínimos: 6.";
    return null;
  }
}

import 'package:flutter/material.dart';
import 'package:gitlab_board/shared/widgets/forms/inputs/input_properties.dart';
import 'package:gitlab_board/shared/widgets/forms/text_field/controllers/text_field_controller.dart';
import 'package:gitlab_board/shared/widgets/helpers/ensure_visible_when_focused.dart';
import 'package:line_icons/line_icons.dart';

class RoundedTextField extends StatefulWidget {
  final TextEditingController textEditingController;
  final FocusNode focusNode;
  final Function onFocusNext;
  final TextInputAction textInputAction;
  final InputProperties inputProperties;

  RoundedTextField({
    Key key,
    @required this.textEditingController,
    @required this.focusNode,
    this.onFocusNext,
    this.textInputAction = TextInputAction.done,
    @required this.inputProperties,
  }) : super(key: key);

  @override
  _RoundedTextFieldState createState() => _RoundedTextFieldState();
}

class _RoundedTextFieldState extends State<RoundedTextField> {
  bool showText = true;
  TextFieldController controller;

  @override
  void initState() {
    showText = !widget.inputProperties.isSecret;
    controller = TextFieldController(
        widget.textEditingController, widget.inputProperties.validator);
    super.initState();
  }

  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[
        Material(
          elevation: 1,
          borderRadius: BorderRadius.circular(50),
          child: EnsureVisibleWhenFocused(
            focusNode: widget.focusNode,
            child: TextFormField(
              controller: controller.textEditingController,
              focusNode: widget.focusNode,
              validator: (String value) {
                controller.validate();
                return controller.validator(value);
              },
              onChanged: (value) {
                controller.isValid(value);
              },
              textInputAction: widget.textInputAction,
              keyboardType: widget.inputProperties.keyboardType,
              onFieldSubmitted: (_) {
                widget.focusNode.unfocus();
                if (widget.onFocusNext != null) widget.onFocusNext();
              },
              obscureText: !showText,
              textCapitalization: widget.inputProperties.textCapitalization,
              decoration: InputDecoration(
                  hintText: widget.inputProperties.placeholder,
                  border: InputBorder.none,
                  prefixIcon: StreamBuilder(
                    stream: controller.isInvalidStream,
                    builder: (_, snapshot) {
                      return getPrefixIcon(snapshot);
                    },
                  ),
                  suffixIcon:
                      widget.inputProperties.isSecret ? _isSecretIcon() : null,
                  errorStyle: TextStyle(
                    height: 0,
                    fontSize: 0,
                  )),
            ),
          ),
        ),
        SizedBox(
          height: 4,
        ),
        Padding(
          padding: const EdgeInsets.only(left: 20.0),
          child: StreamBuilder(
            stream: controller.errorMessageStream,
            builder: (context, snapshot) {
              if (snapshot.hasData && snapshot.data.isNotEmpty) {
                return Text(
                  snapshot.data,
                  style: TextStyle(
                    color: Colors.red[600],
                  ),
                );
              } else {
                return Text("");
              }
            },
          ),
        ),
      ],
    );
  }

  Icon getPrefixIcon(AsyncSnapshot snapshot) {
    Color color = Colors.grey;

    if (snapshot.hasData) {
      if (snapshot.data) {
        color = Colors.green[600];
      } else {
        color = Colors.red[600];
      }
    }

    return Icon(
      widget.inputProperties.prefixIcon,
      color: color,
    );
  }

  Widget _isSecretIcon() => IconButton(
        icon: Icon(
          showText ? LineIcons.eye : LineIcons.eye_slash,
          color: showText ? Theme.of(context).accentColor : Colors.grey,
        ),
        onPressed: _toggleShowText,
      );

  void _toggleShowText() {
    setState(() {
      showText = !showText;
    });
  }
}

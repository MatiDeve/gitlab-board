import 'package:flutter/material.dart';
import 'package:rxdart/rxdart.dart';

class TextFieldController {
  final TextEditingController textEditingController;
  final Function validator;

  TextFieldController(this.textEditingController, this.validator);

  final BehaviorSubject<String> _errorMessageSubject =
      BehaviorSubject<String>.seeded(null);

  Stream<String> get errorMessageStream => _errorMessageSubject.stream;

  final BehaviorSubject<bool> _isInvalidSubject =
      BehaviorSubject<bool>.seeded(null);

  Stream<bool> get isInvalidStream => _isInvalidSubject.stream;

  void isValid(String value) {
    _isInvalidSubject.sink.add(validator(value) == null);
    _errorMessageSubject.sink.add(null);
  }

  void validate() {
    String validateMessage = validator(textEditingController.text);
    _errorMessageSubject.sink.add(validateMessage);
    _isInvalidSubject.sink.add(validateMessage == null);
  }

  void dispose() {
    _errorMessageSubject.close();
    _isInvalidSubject.close();
  }
}

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class RoundedPickerElement {
  final String id;
  final String name;

  RoundedPickerElement(this.id, this.name);
}

class RoundedPicker extends StatefulWidget {
  final List<RoundedPickerElement> list;
  final int actualSelectedItem;
  final Function setSelectedItem;
  final List<FocusNode> unfocusNodes;
  final IconData prefixIcon;

  const RoundedPicker(
      {Key key,
      this.list,
      this.actualSelectedItem,
      this.setSelectedItem,
      this.unfocusNodes,
      this.prefixIcon})
      : super(key: key);

  @override
  _RoundedPickerState createState() => _RoundedPickerState();
}

class _RoundedPickerState extends State<RoundedPicker> {
  FixedExtentScrollController _scrollController;

  @override
  void initState() {
    _scrollController =
        FixedExtentScrollController(initialItem: widget.actualSelectedItem);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () async {
        _unfocusNodes();

        showCupertinoModalPopup<int>(
          context: context,
          builder: (context) {
            int selectedItem = 0;
            return _buildPopup(context, selectedItem);
          },
        ).then((int selectedItem) {
          if (selectedItem != null) {
            widget.setSelectedItem(selectedItem);
          }
        });
      },
      child: _buildRoundedField(),
    );
  }

  void _unfocusNodes() {
    widget.unfocusNodes.forEach((FocusNode f) {
      f.unfocus();
    });
  }

  Column _buildPopup(BuildContext context, int selectedItem) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.end,
      children: <Widget>[
        Container(
          decoration: BoxDecoration(
            color: Color(0xffffffff),
            border: Border(
              bottom: BorderSide(
                color: Color(0xff999999),
                width: 0.0,
              ),
            ),
          ),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              _buildPopupButton(context,
                  title: "Cancelar", returnedValue: () => null),
              _buildPopupButton(context,
                  title: "Confirmar", returnedValue: () => selectedItem),
            ],
          ),
        ),
        Container(
          height: 200.0,
          child: CupertinoPicker(
            scrollController: _scrollController,
            backgroundColor: Colors.white,
            children: widget.list
                .map((element) => Container(
                    height: 50.0, child: Center(child: Text(element.name))))
                .toList(),
            itemExtent: 50.0,
            onSelectedItemChanged: (int index) {
              selectedItem = index;
            },
          ),
        ),
      ],
    );
  }

  CupertinoButton _buildPopupButton(BuildContext context,
      {@required String title, Function returnedValue}) {
    return CupertinoButton(
      child: Text(
        title,
        style: TextStyle(
          color: Theme.of(context).accentColor,
        ),
      ),
      onPressed: () {
        Navigator.of(context).pop(returnedValue());
      },
      padding: const EdgeInsets.symmetric(
        horizontal: 16.0,
        vertical: 5.0,
      ),
    );
  }

  Material _buildRoundedField() {
    return Material(
      elevation: 4,
      borderRadius: BorderRadius.circular(25),
      child: Container(
        height: 48,
        width: double.infinity,
        padding: EdgeInsets.symmetric(horizontal: 12),
        child: Center(
          child: Row(
            children: <Widget>[
              Icon(widget.prefixIcon, color: Colors.green),
              SizedBox(width: 12),
              Expanded(
                  child: Text(widget.list[widget.actualSelectedItem].name)),
              Icon(Icons.arrow_drop_down, color: Colors.grey),
            ],
          ),
        ),
      ),
    );
  }
}

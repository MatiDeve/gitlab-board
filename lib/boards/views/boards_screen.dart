import 'package:flutter/material.dart';
import 'package:gitlab_board/boards/controllers/boards_controller.dart';
import 'package:gitlab_board/boards/models/classes/board.dart';
import 'package:gitlab_board/dependencies_providers/controllers_dependencies.dart';
import 'package:gitlab_board/shared/widgets/helpers/custom_dialog.dart';
import 'package:gitlab_board/shared/widgets/platform/progress_indicator.dart';

import 'board_view.dart';

class BoardsScreen extends StatefulWidget {
  final int projectId;

  const BoardsScreen(this.projectId);

  @override
  _BoardsScreenState createState() => _BoardsScreenState();
}

class _BoardsScreenState extends State<BoardsScreen> {
  final BoardsController _controller =
      ControllersDependencies.getBoardsController();

  @override
  void initState() {
    _controller.errorStream.listen((String message) {
      showDialog(
        context: context,
        child: CustomDialog(
          content: message,
        ),
      );
    });
    _controller.get(widget.projectId);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Boards"),
      ),
      body: StreamBuilder(
        stream: _controller.boardsStream,
        builder: (context, AsyncSnapshot<List<Board>> snapshot) {
          if (snapshot.hasData) {
            if (snapshot.data.isEmpty)
              return _buildNoBoards();
            else
              return ListView.builder(
                padding: EdgeInsets.all(12),
                itemCount: snapshot.data.length,
                itemBuilder: (_, index) =>
                    BoardView(widget.projectId, snapshot.data[index]),
              );
          } else {
            return _buildLoading();
          }
        },
      ),
    );
  }

  Widget _buildLoading() => Padding(
        padding: EdgeInsets.all(12),
        child: Align(
          alignment: Alignment.topCenter,
          child: PlatformProgressIndicator(),
        ),
      );

  Widget _buildNoBoards() => Align(
        alignment: Alignment.topCenter,
        child: Padding(
          padding: const EdgeInsets.all(12.0),
          child: Text(
            "No tienes boards. Crea uno para continuar.",
          ),
        ),
      );
}

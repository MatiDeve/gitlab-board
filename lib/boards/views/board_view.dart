import 'package:flutter/material.dart';
import 'package:gitlab_board/board/views/board_screen.dart';
import 'package:gitlab_board/boards/models/classes/board.dart';

class BoardView extends StatelessWidget {
  final int projectId;
  final Board board;

  BoardView(this.projectId, this.board);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(bottom: 8),
      child: GestureDetector(
        onTap: () {
          Navigator.of(context).push(
            MaterialPageRoute(
              builder: (_) => BoardScreen(projectId,board.id, board.name,
                  board.lists),
            ),
          );
        },
        child: Card(
          elevation: 3,
          child: Padding(
            padding: const EdgeInsets.symmetric(
              vertical: 16.0,
              horizontal: 8,
            ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Text(
                  board.name,
                  style: TextStyle(
                    fontSize: 22,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

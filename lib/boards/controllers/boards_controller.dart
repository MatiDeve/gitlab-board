import 'package:gitlab_board/boards/models/boards_model.dart';
import 'package:gitlab_board/boards/models/classes/board.dart';

class BoardsController {
  final BoardsModel _boardsModel;

  BoardsController(this._boardsModel);

  Stream<List<Board>> get boardsStream => _boardsModel.boardsStream;

  Stream<String> get errorStream => _boardsModel.errorStream;

  void get(int projectId) {
    _boardsModel.get(projectId);
  }
}

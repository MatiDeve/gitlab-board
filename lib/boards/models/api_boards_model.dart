import 'dart:convert';
import 'dart:ui';

import 'package:gitlab_board/shared/classes/access_token_network_handler/access_token_network_handler.dart';
import 'package:gitlab_board/shared/classes/network_handler/classes/network_method.dart';
import 'package:gitlab_board/shared/classes/network_handler/classes/network_request.dart';
import 'package:http/http.dart' as http;
import 'package:rxdart/rxdart.dart';

import 'boards_model.dart';
import 'classes/board.dart';

class ApiBoardsModel implements BoardsModel {
  final AccessTokenNetworkHandler _accessTokenNetworkHandler;

  ApiBoardsModel(this._accessTokenNetworkHandler);

  final PublishSubject<List<Board>> _boardsSubject =
      PublishSubject<List<Board>>();

  final PublishSubject<String> _errorSubject = PublishSubject<String>();

  @override
  void get(int projectId) async {
    NetworkRequest r = NetworkRequest(
      endpoint: "projects/$projectId/boards",
      method: NetworkMethod.GET,
    );
    http.Response response = await _accessTokenNetworkHandler.invoke(r);
    await _sendResponse(response);
  }

  Future _sendResponse(http.Response response) async {
    final List<dynamic> data = json.decode(response.body);

    if (response.statusCode == 200) {
      List<Board> boards = data.map((board) => _jsonToBoard(board)).toList();
      _boardsSubject.sink.add(boards);
    } else {
      _setError("No se pudo obtener los proyectos.");
    }
  }

  Board _jsonToBoard(dynamic board) =>
      Board(board["id"], board["name"], _jsonToBoardLists(board["lists"]));

  List<BoardList> _jsonToBoardLists(List<dynamic> lists) =>
      lists.map(_jsonToBoardList).toList();

  BoardList _jsonToBoardList(dynamic list) =>
      BoardList(list["id"], _jsonToLabel(list["label"]));

  Label _jsonToLabel(dynamic label) => Label(label["id"], label["name"],
      _hexToColor(label["color"]), _hexToColor(label["text_color"]));

  Color _hexToColor(String hexColor) => HexColor.fromHex(hexColor);

  void _setError(String message) {
    _errorSubject.sink.add(message);
  }

  @override
  Stream<List<Board>> get boardsStream => _boardsSubject.stream;

  @override
  Stream<String> get errorStream => _errorSubject.stream;

  @override
  void dispose() {
    _boardsSubject.close();
    _errorSubject.close();
  }
}

extension HexColor on Color {
  /// String is in the format "aabbcc" or "ffaabbcc" with an optional leading "#".
  static Color fromHex(String hexString) {
    final buffer = StringBuffer();
    if (hexString.length == 6 || hexString.length == 7) buffer.write('ff');
    buffer.write(hexString.replaceFirst('#', ''));
    return Color(int.parse(buffer.toString(), radix: 16));
  }

  /// Prefixes a hash sign if [leadingHashSign] is set to `true` (default is `true`).
  String toHex({bool leadingHashSign = true}) => '${leadingHashSign ? '#' : ''}'
      '${alpha.toRadixString(16).padLeft(2, '0')}'
      '${red.toRadixString(16).padLeft(2, '0')}'
      '${green.toRadixString(16).padLeft(2, '0')}'
      '${blue.toRadixString(16).padLeft(2, '0')}';
}

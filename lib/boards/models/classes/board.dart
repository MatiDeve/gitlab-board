import 'dart:ui';

class Board {
  final int id;
  final String name;
  final List<BoardList> lists;

  Board(this.id, this.name, this.lists);
}

class BoardList {
  final int id;
  final Label label;

  BoardList(this.id, this.label);
}

class Label {
  final int id;
  final String name;
  final Color color;
  final Color textColor;

  Label(this.id, this.name, this.color, this.textColor);
}

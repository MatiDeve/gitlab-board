import 'classes/board.dart';

abstract class BoardsModel {
  Stream<List<Board>> get boardsStream;

  Stream<String> get errorStream;

  void get(int projectId);

  void dispose();
}

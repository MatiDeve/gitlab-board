import 'package:flutter/material.dart';
import 'package:gitlab_board/shared/classes/token_handler/token_handler.dart';
import 'package:rxdart/rxdart.dart';

class MainStatusProvider extends ChangeNotifier {
  final TokenHandler _tokenHandler;

  MainStatusProvider(this._tokenHandler) {
    checkIsAuth();
  }

  BehaviorSubject<bool> _isAuthSubject = BehaviorSubject<bool>();

  Stream<bool> get isAuthStream => _isAuthSubject.stream;

  void checkIsAuth() async {
    bool isAuth = await _tokenHandler.exists();
    _isAuthSubject.sink.add(isAuth);
  }

  @override
  void dispose() {
    _isAuthSubject.close();
    super.dispose();
  }
}

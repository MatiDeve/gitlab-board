import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'auth/views/auth_screen.dart';
import 'dependencies_providers/utilities_dependencies.dart';
import 'main_status_provider.dart';
import 'projects/views/projects_screen.dart';

void main() => runApp(MyApp());

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  final MainStatusProvider _mainStatusProvider =
      MainStatusProvider(UtilitiesDependencies.getTokenHandler());

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
      create: (_) => _mainStatusProvider,
      child: MaterialApp(
        color: Colors.blue,
        title: 'Mi UTN',
        theme: ThemeData(
          scaffoldBackgroundColor: Colors.white,
          backgroundColor: Colors.white,
          primarySwatch: Colors.grey,
          primaryColor: Colors.brown[800],
          accentColor: Color(0xFFFCA326),
          buttonTheme: ButtonThemeData(
            buttonColor: Colors.brown[800],
            textTheme: ButtonTextTheme.primary,
          ),
          textTheme: TextTheme(
            body1: TextStyle(
              fontSize: 16,
            ),
            title: TextStyle(
              fontSize: 22,
              fontWeight: FontWeight.bold,
            ),
            subtitle: TextStyle(
              fontSize: 16,
              fontWeight: FontWeight.w400,
            ),
            headline: TextStyle(
              fontSize: 20,
              fontWeight: FontWeight.w700,
              height: 1.6,
              fontFamily: "SuecaSlabMedium",
            ),
            display2: TextStyle(
              color: Colors.black,
              fontWeight: FontWeight.w400,
              height: 1.4,
              fontSize: 18,
            ),
            display3: TextStyle(
              fontSize: 15,
              fontWeight: FontWeight.w400,
              color: Color(0xFF484848),
              fontStyle: FontStyle.italic,
            ),
            display4: TextStyle(
              fontSize: 13,
              fontWeight: FontWeight.w400,
              color: Colors.grey[600],
            ),
          ),
          dividerTheme: DividerThemeData(color: Colors.grey[400], space: 10),
        ),
        home: StreamBuilder(
          stream: _mainStatusProvider.isAuthStream,
          builder: (context, snapshot) {
            if (snapshot.hasData) {
              return !snapshot.data ? AuthScreen() : ProjectsScreen();
            } else {
              return Center(
                child: Text(
                  "Loading",
                ),
              );
            }
          },
        ),
      ),
    );
  }
}

class LogoutButton extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    MainStatusProvider mainStatusProvider =
        Provider.of<MainStatusProvider>(context, listen: false);
    return IconButton(
      icon: Icon(Icons.exit_to_app),
      onPressed: () {
        UtilitiesDependencies.getTokenHandler().destroy();
        mainStatusProvider.checkIsAuth();
      },
    );
  }
}

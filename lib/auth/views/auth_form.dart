import 'package:flutter/material.dart';
import 'package:gitlab_board/auth/controllers/auth_form_controller.dart';
import 'package:gitlab_board/dependencies_providers/controllers_dependencies.dart';
import 'package:gitlab_board/main_status_provider.dart';
import 'package:gitlab_board/shared/urls.dart';
import 'package:gitlab_board/shared/widgets/forms/inputs/access_token_input_properties.dart';
import 'package:gitlab_board/shared/widgets/forms/text_field/views/linear_text_field.dart';
import 'package:gitlab_board/shared/widgets/helpers/custom_dialog.dart';
import 'package:gitlab_board/shared/widgets/helpers/link.dart';
import 'package:gitlab_board/shared/widgets/platform/progress_indicator.dart';
import 'package:provider/provider.dart';

import '../../shared/widgets/forms/rounded_confirm_button.dart';
import '../../shared/widgets/helpers/icon_medium_size.dart';

class AuthForm extends StatefulWidget {
  const AuthForm({
    Key key,
  }) : super(key: key);

  @override
  _AuthFormState createState() => _AuthFormState();
}

class _AuthFormState extends State<AuthForm> {
  bool waitingForRequest = false;
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  final TextEditingController _accessTokenTextEditingController =
      TextEditingController();
  final _accessTokenFocusNode = FocusNode();

  AuthFormController _controller;

  @override
  void initState() {
    final mainStatusProvider =
        Provider.of<MainStatusProvider>(context, listen: false);
    _controller = ControllersDependencies.getAuthFormController();

    _controller.isLoggedStream.listen((isLogged) {
      if (mounted)
        setState(() {
          waitingForRequest = false;
        });

      if (isLogged) mainStatusProvider.checkIsAuth();
    });

    _controller.loginErrorMessageStream.listen((errorMessage) {
      showDialog(
        context: context,
        child: CustomDialog(
          content: errorMessage,
        ),
      );
    });

    super.initState();
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Form(
      key: _formKey,
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          IconMediumSize(
            assetPath: "assets/icon/auth_icon.png",
          ),
          SizedBox(
            height: 32,
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 16.0),
            child: _buildAccessTokenTextField(),
          ),
          SizedBox(
            height: 8,
          ),
          SizedBox(
            height: 16.0,
          ),
          waitingForRequest
              ? Container(
                  height: 50,
                  child: PlatformProgressIndicator(),
                )
              : RoundedConfirmButton(
                  text: "Entrar",
                  onPressed: _confirm,
                ),
          SizedBox(
            height: 20.0,
          ),
          Text(
            "Al ingresar, estás de acuerdo con las ",
          ),
          Link(
            text: "políticas de privacidad",
            url: privacyUrl,
          ),
        ],
      ),
    );
  }

  Widget _buildAccessTokenTextField() => LinearTextField(
        textEditingController: _accessTokenTextEditingController,
        focusNode: _accessTokenFocusNode,
        textInputAction: TextInputAction.next,
        inputProperties: AccessTokenInputProperties(),
      );

  void _confirm() {
    if (_formKey.currentState.validate()) {
      setState(() {
        waitingForRequest = true;
      });
      _controller.login(_accessTokenTextEditingController.text);
    }
  }
}

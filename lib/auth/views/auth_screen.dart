import 'package:flutter/material.dart';

import 'auth_form.dart';

class AuthScreen extends StatelessWidget {
  static String routeName = "/";

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Padding(
        padding: EdgeInsets.all(12),
        child: SingleChildScrollView(
          child: Container(
            height: MediaQuery.of(context).size.height - 50,
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Flexible(
                  child: Container(),
                ),
                Padding(
                  padding: const EdgeInsets.all(12.0),
                  child: AuthForm(),
                ),
                Flexible(
                  child: Container(),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

import 'package:gitlab_board/auth/models/auth_form_model.dart';
import 'package:gitlab_board/auth/models/classes/auth_user.dart';

class AuthFormController {
  final AuthFormModel _model;

  AuthFormController(this._model);

  Stream<bool> get isLoggedStream => _model.isLoggedStream;

  Stream<String> get loginErrorMessageStream => _model.loginErrorMessageStream;

  void login(String accessToken) {
    var user = AuthUser(accessToken);
    _model.login(user);
  }

  void dispose() {
    _model.dispose();
  }
}

import 'classes/auth_user.dart';

abstract class AuthFormModel {
  Stream<bool> get isLoggedStream;

  Stream<String> get loginErrorMessageStream;

  void login(AuthUser u);

  void dispose();
}

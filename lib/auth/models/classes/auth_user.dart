class AuthUser {
  final String token;

  AuthUser(this.token);

  Map<String, dynamic> toMap() => {
        "accessToken": token,
      };
}

import 'dart:convert';

import 'package:gitlab_board/auth/models/classes/auth_user.dart';
import 'package:gitlab_board/shared/classes/network_handler/classes/network_method.dart';
import 'package:gitlab_board/shared/classes/network_handler/classes/network_request.dart';
import 'package:gitlab_board/shared/classes/network_handler/network_handler.dart';
import 'package:gitlab_board/shared/classes/token_handler/token_handler.dart';
import 'package:gitlab_board/shared/classes/user_handler/user_handler.dart';
import 'package:http/http.dart' as http;
import 'package:rxdart/rxdart.dart';

import 'auth_form_model.dart';

class ApiAuthFormModel implements AuthFormModel {
  final NetworkHandler _networkHandler;
  final TokenHandler _tokenHandler;
  final UserHandler _userHandler;

  ApiAuthFormModel(this._networkHandler, this._tokenHandler, this._userHandler);

  final PublishSubject<bool> _isLoggedSubject = PublishSubject<bool>();
  final PublishSubject<String> _loginErrorMessageSubject =
      PublishSubject<String>();

  @override
  Stream<bool> get isLoggedStream => _isLoggedSubject.stream;

  @override
  Stream<String> get loginErrorMessageStream =>
      _loginErrorMessageSubject.stream;

  @override
  void login(AuthUser u) async {
    var data = u.toMap();
    NetworkRequest r = NetworkRequest(
        endpoint: "user?access_token=${u.token}",
        method: NetworkMethod.GET,
        data: data);
    http.Response response = await _networkHandler.invoke(r);
    await _sendLoginResponse(response, u.token);
  }

  Future _sendLoginResponse(http.Response response, String token) async {
    final Map<String, dynamic> data = json.decode(response.body);

    if (response.statusCode == 200) {
      _userHandler.set(User(data["id"], data["name"], data["username"]));

      if (!await _userHandler.exists()) {
        _userHandler.destroy();
        _setError("No se pudo almacenar el usuario correctamente.");
      } else {
        _tokenHandler.set(token);
        _isLoggedSubject.sink.add(true);
      }
    } else {
      _setError(data["message"]);
    }
  }

  void _setError(String message) {
    _loginErrorMessageSubject.sink.add(message);
    _isLoggedSubject.sink.add(false);
  }

  @override
  void dispose() {
    _isLoggedSubject.close();
    _loginErrorMessageSubject.close();
  }
}

import 'package:flutter/material.dart';
import 'package:gitlab_board/board/models/board_model.dart';
import 'package:gitlab_board/board/models/classes/board_column.dart';
import 'package:gitlab_board/board/models/classes/board_data.dart';
import 'package:gitlab_board/board/models/classes/issue.dart';
import 'package:gitlab_board/boards/models/classes/board.dart';
import 'package:gitlab_board/dependencies_providers/controllers_dependencies.dart';
import 'package:gitlab_board/shared/widgets/helpers/custom_dialog.dart';
import 'package:gitlab_board/shared/widgets/ui/loading.dart';

class BoardScreen extends StatefulWidget {
  final int projectId;
  final int boardId;
  final String boardName;
  final List<BoardList> boardLists;

  const BoardScreen(
      this.projectId, this.boardId, this.boardName, this.boardLists);

  @override
  _BoardScreenState createState() => _BoardScreenState();
}

class _BoardScreenState extends State<BoardScreen> {
  final BoardController _controller =
      ControllersDependencies.getBoardController();

  @override
  void initState() {
    _controller.errorStream.listen((String message) {
      showDialog(
        context: context,
        child: CustomDialog(
          content: message,
        ),
      );
    });
    _controller.get(widget.projectId, widget.boardLists);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.boardName),
      ),
      body: StreamBuilder(
        stream: _controller.boardDataStream,
        builder: (_, AsyncSnapshot<BoardData> s) {
          if (!s.hasData) {
            return Loading();
          } else {
            return BoardDataView(s.data);
          }
        },
      ),
    );
  }
}

class BoardDataView extends StatefulWidget {
  final BoardData _boardData;

  BoardDataView(this._boardData);

  @override
  _BoardDataViewState createState() => _BoardDataViewState();
}

class _BoardDataViewState extends State<BoardDataView> {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: SingleChildScrollView(
        scrollDirection: Axis.horizontal,
        child: Container(
          child: Padding(
            padding: const EdgeInsets.fromLTRB(12, 12, 12, 0),
            child: Row(
              children: _buildColumns(context),
            ),
          ),
        ),
      ),
    );
  }

  List<Widget> _buildColumns(BuildContext context) =>
      [_buildOpened(context)] +
      _buildBoardLists(context) +
      [_buildClosed(context)];

  Widget _buildOpened(BuildContext context) {
    return DragTarget<IssueAndList>(
      builder: (BuildContext context, List candidateData, List rejectedData) {
        return Container(
          width: MediaQuery.of(context).size.width * 0.8,
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Text("Opened"),
              Expanded(
                child: ListView.builder(
                  itemCount: widget._boardData.opened.length,
                  itemBuilder: (BuildContext context, int index) {
                    return IssueView(widget._boardData.opened[index]);
                  },
                ),
              ),
            ],
          ),
        );
      },
      onWillAccept: (_) => true,
      onAccept: (data) {
        setState(() {
          widget._boardData.updateIssueLabel(data.issue, data.listId, null,
              toState: IssueState.opened);
        });
      },
    );
  }

  List<Widget> _buildBoardLists(BuildContext context) {
    return widget._boardData.columns
        .map((column) => _buildBoardList(context, column))
        .toList();
  }

  Widget _buildBoardList(BuildContext context, BoardColumn column) {
    return DragTarget<IssueAndList>(
      builder: (BuildContext context, List candidateData, List rejectedData) {
        return Container(
          width: MediaQuery.of(context).size.width * 0.8,
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Text(column.list.label.name),
              Expanded(
                child: ListView.builder(
                  itemCount: column.issues.length,
                  itemBuilder: (BuildContext context, int index) {
                    return IssueView(column.issues[index],
                        listId: column.list.id);
                  },
                ),
              ),
            ],
          ),
        );
      },
      onWillAccept: (_) => true,
      onAccept: (data) {
        setState(() {
          widget._boardData
              .updateIssueLabel(data.issue, data.listId, column.list.id);
        });
      },
    );
  }

  Widget _buildClosed(BuildContext context) {
    return DragTarget<IssueAndList>(
      builder: (BuildContext context, List candidateData, List rejectedData) {
        return Container(
          width: MediaQuery.of(context).size.width * 0.8,
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Text("Closed"),
              Expanded(
                child: ListView.builder(
                  itemCount: widget._boardData.closed.length,
                  itemBuilder: (BuildContext context, int index) {
                    return IssueView(widget._boardData.closed[index]);
                  },
                ),
              ),
            ],
          ),
        );
      },
      onWillAccept: (_) => true,
      onAccept: (data) {
        setState(() {
          widget._boardData.updateIssueLabel(data.issue, data.listId, null,
              toState: IssueState.closed);
        });
      },
    );
  }
}

class IssueView extends StatelessWidget {
  final Issue _issue;
  final int listId;

  IssueView(this._issue, {this.listId});

  @override
  Widget build(BuildContext context) {
    return Draggable<IssueAndList>(
      childWhenDragging: Container(),
      feedback: Container(
        width: MediaQuery.of(context).size.width * 0.8,
        child: Card(
          child: Padding(
            padding: EdgeInsets.all(12),
            child: Text(_issue.title),
          ),
        ),
      ),
      data: IssueAndList(_issue, listId),
      child: Card(
        child: Padding(
          padding: EdgeInsets.all(12),
          child: Text(_issue.title),
        ),
      ),
    );
  }
}

class IssueAndList {
  final Issue issue;
  final int listId;

  IssueAndList(this.issue, this.listId);
}

import 'dart:convert';
import 'dart:convert' show utf8;

import 'package:gitlab_board/boards/models/classes/board.dart';
import 'package:gitlab_board/shared/classes/access_token_network_handler/access_token_network_handler.dart';
import 'package:gitlab_board/shared/classes/network_handler/classes/network_method.dart';
import 'package:gitlab_board/shared/classes/network_handler/classes/network_request.dart';
import 'package:http/http.dart' as http;
import 'package:rxdart/rxdart.dart';

import 'board_model.dart';
import 'classes/board_data.dart';
import 'classes/issue.dart';

class ApiBoardModel implements BoardModel {
  final AccessTokenNetworkHandler _accessTokenNetworkHandler;

  ApiBoardModel(this._accessTokenNetworkHandler);

  final PublishSubject<BoardData> _boardDataSubject =
  PublishSubject<BoardData>();
  final PublishSubject<String> _errorSubject = PublishSubject<String>();

  @override
  Stream<BoardData> get boardDataStream => _boardDataSubject.stream;

  @override
  Stream<String> get errorStream => _errorSubject.stream;

  @override
  void get(int projectId, List<BoardList> boardLists) async {
    NetworkRequest r = NetworkRequest(
      endpoint: "projects/$projectId/issues",
      method: NetworkMethod.GET,
    );
    http.Response response = await _accessTokenNetworkHandler.invoke(r);
    await _sendResponse(response, boardLists);
  }

  Future _sendResponse(
      http.Response response, List<BoardList> boardLists) async {
    if (response.statusCode == 200) {
      final List<dynamic> data = json.decode(Utf8Decoder().convert(response.bodyBytes));
      List<Issue> issues = _jsonToIssues(data);

      BoardData boardData = BoardData(boardLists);
      boardData.addIssues(issues);
      boardData.printData();
      _boardDataSubject.sink.add(boardData);
    } else {
      _setError("No se pudo obtener los proyectos.");
    }
  }

  void _setError(String message) {
    _errorSubject.sink.add(message);
  }

  @override
  void dispose() {
    _boardDataSubject.close();
    _errorSubject.close();
  }

  List<Issue> _jsonToIssues(List<dynamic> issues) =>
      issues.map(_jsonToIssue).toList();

  Issue _jsonToIssue(dynamic issue) => Issue(
    issue["id"],
    issue["iid"],
    issue["title"],
    issue["description"],
    _stringToState(issue["state"]),
    _jsonToListString(issue["labels"]),
  );

  IssueState _stringToState(state) =>
      state == "closed" ? IssueState.closed : IssueState.opened;

  List<String> _jsonToListString(List<dynamic> labels) =>
      labels.map<String>((label) => label).toList();
}
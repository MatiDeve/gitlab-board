import 'package:gitlab_board/boards/models/classes/board.dart';

import 'classes/board_data.dart';

class BoardController {
  final BoardModel _boardModel;

  BoardController(this._boardModel);

  Stream<BoardData> get boardDataStream => _boardModel.boardDataStream;

  Stream<String> get errorStream => _boardModel.errorStream;

  void get(int projectId, List<BoardList> b) {
    _boardModel.get(projectId, b);
  }
}

abstract class BoardModel {
  Stream<BoardData> get boardDataStream;

  Stream<String> get errorStream;

  void get(int projectId, List<BoardList> boardLists);

  void dispose();
}
import 'package:gitlab_board/boards/models/classes/board.dart';

import 'issue.dart';

class BoardColumn {
  final BoardList list;
  final List<Issue> _issues = [];

  BoardColumn(this.list);

  void addIssue(Issue issue) {
    _issues.add(issue);
  }

  void printData() {
    print("\n");
    print("${list.label.name}");
    _issues.forEach((issue) {
      issue.printData();
    });
  }

  List<Issue> get issues => List.from(_issues);

  void removeIssue(Function where) {
    _issues.removeWhere(where);
  }
}

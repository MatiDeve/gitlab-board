enum IssueState { opened, closed }

class Issue {
  final int id;
  final int iid;
  final String title;
  final String description;
  IssueState state;
  final List<String> labels;

  Issue(
      this.id, this.iid, this.title, this.description, this.state, this.labels);

  void printData() {
    print("\n");
    print("id: $id");
    print("iid: $iid");
    print("title: $title");
    print("description: $description");
    print("state: $state");
    print("labels: $labels");
    print("\n");
  }
}
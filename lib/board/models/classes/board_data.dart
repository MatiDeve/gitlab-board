import 'package:gitlab_board/boards/models/classes/board.dart';

import 'board_column.dart';
import 'issue.dart';

class BoardData {
  final List<Issue> opened = [];
  final List<BoardColumn> columns = [];
  final List<Issue> closed = [];

  BoardData(List<BoardList> lists) {
    lists.forEach((list) => {columns.add(BoardColumn(list))});
  }

  void printData() {
    print("opened");
    opened.forEach((issue) {
      issue.printData();
    });
    columns.forEach((column) {
      column.printData();
    });
    print("\n");
    print("closed");
    closed.forEach((issue) {
      issue.printData();
    });
  }

  void addIssues(List<Issue> issues) => issues.forEach(addIssue);

  void addIssue(Issue i) {
    var filteredColumns = columns
        .where((column) => i.labels.contains(column.list.label.name))
        .toList();

    if (filteredColumns.isNotEmpty)
      filteredColumns.forEach((column) {
        column.addIssue(i);
      });
    else
      _addIssueToOpenedOrClosed(i);
  }

  void _addIssueToOpenedOrClosed(Issue i) {
    switch (i.state) {
      case IssueState.closed:
        closed.add(i);
        return;
      default:
        opened.add(i);
        return;
    }
  }

  void updateIssueLabel(Issue i, int fromId, int toId, {IssueState toState}) {
    if (fromId == null) {
      _removeFromOpenedOrClosed(i);
    } else {
      int fromIndex = _getIndexForListId(fromId);
      i.labels
          .removeWhere((label) => label == columns[fromIndex].list.label.name);

      _removeFromList(i, fromId);
    }

    if (toId != null) {
      int toIndex = _getIndexForListId(toId);
      i.labels.add(columns[toIndex].list.label.name);
      addIssue(i);
    }

    if (toState != null) {
      i.state = toState;
      addIssue(i);
    }
  }

  int _getIndexForListId(int toId) =>
      columns.indexWhere((column) => column.list.id == toId);

  void _removeFromList(Issue i, int listId) {
    i.printData();
    columns
        .firstWhere((column) => column.list.id == listId)
        .removeIssue((issue) {
      return issue.id == i.id;
    });
  }

  void _removeFromOpenedOrClosed(Issue i) {
    switch (i.state) {
      case IssueState.closed:
        closed.removeWhere((issue) => issue.id == i.id);
        break;
      default:
        opened.removeWhere((issue) => issue.id == i.id);
        break;
    }
  }
}

import 'package:gitlab_board/board/models/board_model.dart';
import 'package:gitlab_board/board/models/classes/board_data.dart';
import 'package:gitlab_board/boards/models/classes/board.dart';

class BoardController {
  final BoardModel _boardModel;

  BoardController(this._boardModel);

  Stream<BoardData> get boardDataStream => _boardModel.boardDataStream;

  Stream<String> get errorStream => _boardModel.errorStream;

  void get(int projectId, List<BoardList> b) {
    _boardModel.get(projectId, b);
  }
}
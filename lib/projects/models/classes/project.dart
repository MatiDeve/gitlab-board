import 'package:gitlab_board/dependencies_providers/utilities_dependencies.dart';

class Project {
  final int id;
  final String name;
  final DateTime lastActivityAt;

  Project(this.id, this.name, this.lastActivityAt);

  String get difference =>
      UtilitiesDependencies.getTimeDifferenceProvider().get(lastActivityAt);
}

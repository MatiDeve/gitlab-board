import 'classes/project.dart';

abstract class ProjectsModel {
  Stream<List<Project>> get projectsStream;

  Stream<String> get errorStream;

  void get();

  void dispose();
}

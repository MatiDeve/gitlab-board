import 'dart:convert';

import 'package:gitlab_board/shared/classes/access_token_network_handler/access_token_network_handler.dart';
import 'package:gitlab_board/shared/classes/network_handler/classes/network_method.dart';
import 'package:gitlab_board/shared/classes/network_handler/classes/network_request.dart';
import 'package:gitlab_board/shared/classes/user_handler/user_handler.dart';
import 'package:http/http.dart' as http;
import 'package:rxdart/rxdart.dart';

import 'classes/project.dart';
import 'projects_model.dart';

class ApiProjectsModel implements ProjectsModel {
  final AccessTokenNetworkHandler _accessTokenNetworkHandler;
  final UserHandler _userHandler;

  ApiProjectsModel(this._accessTokenNetworkHandler, this._userHandler);

  final PublishSubject<List<Project>> _projectsSubject =
      PublishSubject<List<Project>>();

  final PublishSubject<String> _errorSubject = PublishSubject<String>();

  @override
  void get() async {
    User user = await _userHandler.get();
    NetworkRequest r = NetworkRequest(
      endpoint: "users/${user.id}/projects",
      method: NetworkMethod.GET,
    );
    http.Response response = await _accessTokenNetworkHandler.invoke(r);
    await _sendResponse(response);
  }

  Future _sendResponse(http.Response response) async {
    final List<dynamic> data = json.decode(response.body);

    if (response.statusCode == 200) {
      List<Project> projects =
          data.map((project) => _jsonToProject(project)).toList();
      _projectsSubject.sink.add(projects);
    } else {
      _setError("No se pudo obtener los proyectos.");
    }
  }

  Project _jsonToProject(dynamic project) => Project(project["id"],
      project["name"], _stringToDate(project["last_activity_at"]));

  DateTime _stringToDate(String date) => DateTime.parse(date);

  void _setError(String message) {
    _errorSubject.sink.add(message);
  }

  @override
  Stream<List<Project>> get projectsStream => _projectsSubject.stream;

  @override
  Stream<String> get errorStream => _errorSubject.stream;

  @override
  void dispose() {
    _projectsSubject.close();
    _errorSubject.close();
  }
}

import 'package:gitlab_board/projects/models/classes/project.dart';
import 'package:gitlab_board/projects/models/projects_model.dart';

class ProjectsController {
  final ProjectsModel _projectsModel;

  ProjectsController(this._projectsModel);

  Stream<List<Project>> get projectsStream => _projectsModel.projectsStream;

  Stream<String> get errorStream => _projectsModel.errorStream;

  void get() {
    _projectsModel.get();
  }
}

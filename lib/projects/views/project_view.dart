import 'package:flutter/material.dart';
import 'package:gitlab_board/boards/views/boards_screen.dart';
import 'package:gitlab_board/projects/models/classes/project.dart';

class ProjectView extends StatelessWidget {
  final Project project;

  ProjectView(this.project);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(bottom: 8),
      child: GestureDetector(
        onTap: () {
          Navigator.of(context).push(
            MaterialPageRoute(
              builder: (_) => BoardsScreen(project.id),
            ),
          );
        },
        child: Card(
          elevation: 3,
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Text(
                  project.name,
                  style: TextStyle(
                    fontSize: 22,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                Chip(
                  label: Text(
                    "🕒️  ${project.difference}",
                    style: TextStyle(
                      fontWeight: FontWeight.w500,
                    ),
                    overflow: TextOverflow.fade,
                  ),
                  labelPadding: EdgeInsets.all(0),
                  padding: EdgeInsets.symmetric(horizontal: 12),
                  backgroundColor: Theme.of(context).accentColor,
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

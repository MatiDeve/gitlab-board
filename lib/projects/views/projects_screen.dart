import 'package:flutter/material.dart';
import 'package:gitlab_board/dependencies_providers/controllers_dependencies.dart';
import 'package:gitlab_board/projects/controllers/projects_controller.dart';
import 'package:gitlab_board/projects/models/classes/project.dart';
import 'package:gitlab_board/shared/widgets/helpers/custom_dialog.dart';
import 'package:gitlab_board/shared/widgets/platform/progress_indicator.dart';

import '../../main.dart';
import 'project_view.dart';

class ProjectsScreen extends StatefulWidget {
  @override
  _ProjectsScreenState createState() => _ProjectsScreenState();
}

class _ProjectsScreenState extends State<ProjectsScreen> {
  ProjectsController _controller;

  @override
  void initState() {
    _controller = ControllersDependencies.getProjectsController();

    _controller.errorStream.listen((String message) {
      showDialog(
        context: context,
        child: CustomDialog(
          content: message,
        ),
      );
    });

    _controller.get();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Proyectos"),
        actions: <Widget>[LogoutButton()],
      ),
      body: StreamBuilder(
        stream: _controller.projectsStream,
        builder: (context, AsyncSnapshot<List<Project>> snapshot) {
          if (snapshot.hasData) {
            if (snapshot.data.isEmpty)
              return _buildNoProjects();
            else
              return ListView.builder(
                padding: EdgeInsets.all(12),
                itemCount: snapshot.data.length,
                itemBuilder: (_, index) => ProjectView(snapshot.data[index]),
              );
          } else {
            return _buildLoading();
          }
        },
      ),
    );
  }

  Widget _buildLoading() => Padding(
        padding: EdgeInsets.all(12),
        child: Align(
          alignment: Alignment.topCenter,
          child: PlatformProgressIndicator(),
        ),
      );

  Widget _buildNoProjects() => Align(
        alignment: Alignment.topCenter,
        child: Padding(
          padding: const EdgeInsets.all(12.0),
          child: Text(
            "No tienes proyectos. Crea uno para continuar.",
          ),
        ),
      );
}

import 'package:gitlab_board/shared/classes/access_token_network_handler/access_token_network_handler.dart';
import 'package:gitlab_board/shared/classes/local_store/local_store.dart';
import 'package:gitlab_board/shared/classes/local_store/shared_preferences_local_store.dart';
import 'package:gitlab_board/shared/classes/network_handler/network_handler.dart';
import 'package:gitlab_board/shared/classes/plain_text_converter/decoders/img_decoder.dart';
import 'package:gitlab_board/shared/classes/plain_text_converter/decoders/url_decoder.dart';
import 'package:gitlab_board/shared/classes/plain_text_converter/plain_text_converter.dart';
import 'package:gitlab_board/shared/classes/remember_email_handler/remember_email_hander.dart';
import 'package:gitlab_board/shared/classes/remember_email_handler/shared_preferences_remember_email_handler.dart';
import 'package:gitlab_board/shared/classes/time_difference/time_difference.dart';
import 'package:gitlab_board/shared/classes/token_handler/shared_preferences_token_handler.dart';
import 'package:gitlab_board/shared/classes/token_handler/token_handler.dart';
import 'package:gitlab_board/shared/classes/user_handler/shared_preferences_user_handler.dart';
import 'package:gitlab_board/shared/classes/user_handler/user_handler.dart';
import 'package:gitlab_board/shared/urls.dart';

class UtilitiesDependencies {
  static LocalStore getLocalStoreProvider() => SharedPreferencesLocalStore();

  static RememberEmailHandler getRememberEmailHandler() =>
      SharedPreferencesRememberEmailHandler(getLocalStoreProvider());

  static TimeDifference getTimeDifferenceProvider() => TimeDifference();

  static TokenHandler getTokenHandler() =>
      SharedPreferencesTokenHandler(getLocalStoreProvider());

  static UserHandler getUserHandler() =>
      SharedPreferencesUserHandler(getLocalStoreProvider());

  static PlainTextConverter getPlainTextConverter() =>
      PlainTextConverter([ImgDecoder(), UrlDecoder()]);

  static NetworkHandler getNetworkHandler() => NetworkHandler(apiUrl);

  static AccessTokenNetworkHandler getAccessTokenNetworkHandler() =>
      AccessTokenNetworkHandler(getNetworkHandler(), getTokenHandler());
}

import 'package:gitlab_board/auth/models/api_auth_form_model.dart';
import 'package:gitlab_board/auth/models/auth_form_model.dart';
import 'package:gitlab_board/board/models/api_board_model.dart';
import 'package:gitlab_board/board/models/board_model.dart';
import 'package:gitlab_board/boards/models/api_boards_model.dart';
import 'package:gitlab_board/boards/models/boards_model.dart';
import 'package:gitlab_board/dependencies_providers/utilities_dependencies.dart';
import 'package:gitlab_board/projects/models/api_projects_model.dart';
import 'package:gitlab_board/projects/models/projects_model.dart';
import 'package:gitlab_board/shared/classes/access_token_network_handler/access_token_network_handler.dart';
import 'package:gitlab_board/shared/classes/network_handler/network_handler.dart';
import 'package:gitlab_board/shared/classes/token_handler/token_handler.dart';
import 'package:gitlab_board/shared/classes/user_handler/user_handler.dart';

class ModelsDependencies {
  static NetworkHandler _networkHandler =
      UtilitiesDependencies.getNetworkHandler();
  static TokenHandler _tokenHandler = UtilitiesDependencies.getTokenHandler();
  static UserHandler _userHandler = UtilitiesDependencies.getUserHandler();
  static AccessTokenNetworkHandler _accessTokenNetworkHandler =
      UtilitiesDependencies.getAccessTokenNetworkHandler();

  static AuthFormModel getAuthFormModel() =>
      ApiAuthFormModel(_networkHandler, _tokenHandler, _userHandler);

  static ProjectsModel getProjectsModel() =>
      ApiProjectsModel(_accessTokenNetworkHandler, _userHandler);

  static BoardsModel getBoardsModel() =>
      ApiBoardsModel(_accessTokenNetworkHandler);

  static BoardModel getBoardModel() =>
      ApiBoardModel(_accessTokenNetworkHandler);
}

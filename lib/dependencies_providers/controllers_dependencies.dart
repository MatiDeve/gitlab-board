import 'package:gitlab_board/auth/controllers/auth_form_controller.dart';
import 'package:gitlab_board/board/models/board_model.dart';
import 'package:gitlab_board/boards/controllers/boards_controller.dart';
import 'package:gitlab_board/dependencies_providers/models_dependencies.dart';
import 'package:gitlab_board/projects/controllers/projects_controller.dart';

class ControllersDependencies {
  static AuthFormController getAuthFormController() =>
      AuthFormController(ModelsDependencies.getAuthFormModel());

  static ProjectsController getProjectsController() =>
      ProjectsController(ModelsDependencies.getProjectsModel());

  static BoardsController getBoardsController() =>
      BoardsController(ModelsDependencies.getBoardsModel());

  static BoardController getBoardController() =>
      BoardController(ModelsDependencies.getBoardModel());
}
